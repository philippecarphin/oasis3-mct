#
# Include file for OASIS3 Makefile for a Linux system using
# PGI and OPENMPI
#
###############################################################################
#
# CHAN	: communication technique used in OASIS3 (MPI1/MPI2)
CHAN            = MPI1
#
# Paths for libraries, object files and binaries
#
# COUPLE	: path for oasis3-mct main directory
COUPLE          = $(COUPLE_ENV_VAR)
#
# ARCHDIR       : directory created when compiling
PREFIX ?= $(COUPLE)/oasis_build

ARCHDIR         = $(PREFIX)
#
# MPI library
MPIDIR      = $(MPIDIR_ENV_VAR)
MPIBIN      = $(MPIDIR)/bin
MPI_INCLUDE = $(MPIDIR)/include
MPILIB      = -L$(MPIDIR)/lib
#
# NETCDF library
NETCDF_BASE = /opt/cray/pe/netcdf/4.6.1.3
NETCDF_FORTRAN = /fs/ssm/hpco/exp/hdf5-netcdf4/master/netcdf-fortran_4.4.5-intel-19.0.3.199-openmpi-3.1.2-static_ubuntu-18.04-amd64-64
NETCDF_INCLUDE  = -I $(NETCDF_BASE)/include  -I $(NETCDF_FORTRAN)/include
NETCDF_LDFLAGS = -L$(NETCDF_BASE)/lib -L$(NETCDF_FORTRAN)/lib
NETCDF_LDLIBS  = -lnetcdff -lnetcdf
# NETCDF_LDFLAGS  = -L$(NETCDF_LIB) -Wl,-rpath,$(NETCDF_LIB) -lnetcdff  -Wl,-rpath,/usr/local/netcdf-c/4.6.1/lib  -L/usr/local/netcdf-c/4.6.1/lib -lnetcdf
#
# Compiling and other commands
MAKE        = make
F90         = ftn
F           = $(F90)
f90         = $(F90)
f           = $(F90)
CC          = cc
LD          = ld
AR          = ar
ARFLAGS     = -ruv
#
# CPP keys and compiler options
#
CPPDEF    = -Duse_comm_$(CHAN) -D__VERBOSE -DTREAT_OVERLAY -D__NO_16BYTE_REALS
#
# -g is necessary in F90FLAGS and LDFLAGS for pgf90 versions lower than 6.1
#
F90FLAGS_1  = -C -g -lpthread
f90FLAGS_1  = $(F90FLAGS_1)
FFLAGS_1    = $(F90FLAGS_1)
fFLAGS_1    = $(F90FLAGS_1)
CCFLAGS_1   =
LDFLAGS     =
#
###################
#
# Additional definitions that should not be changed
#
FLIBS		= $(NETCDF_LDFLAGS) $(NETCDF_LDLIBS)
# BINDIR        : directory for executables
BINDIR          = $(ARCHDIR)/bin
# LIBBUILD      : contains a directory for each library
LIBBUILD        = $(ARCHDIR)/lib
# INCPSMILE     : includes all *o and *mod for each library
INCPSMILE       = -I$(ARCHDIR)/include

F90FLAGS  = $(F90FLAGS_1) $(INCPSMILE) $(CPPDEF)
f90FLAGS  = $(f90FLAGS_1) $(INCPSMILE) $(CPPDEF)
FFLAGS    = $(FFLAGS_1) $(INCPSMILE) $(CPPDEF)
fFLAGS    = $(fFLAGS_1) $(INCPSMILE) $(CPPDEF)
CCFLAGS   = $(CCFLAGS_1) $(INCPSMILE) $(CPPDEF)
#
#############################################################################
