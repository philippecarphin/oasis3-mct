
# OBJ_M1 =  read_grid.o def_parallel_decomposition.o
# OBJ_M2 =  read_grid.o def_parallel_decomposition_m2.o
# ocean: $(LIBPSMILE) $(ocean_obj) Makefile
# 	$(call make_echo_link_fortran_executable)
# 	$(at) ftn $(LDFLAGS) -o $@ $(ocean_obj) $(LIBPSMILE) $(FLIBS)
# atmos: $(LIBPSMILE) $(atmos_obj) Makefile
# 	$(call make_echo_link_fortran_executable)
# 	$(at) ftn $(LDFLAGS) -o $@ $(atmos_obj) $(LIBPSMILE) $(FLIBS)

add_library(readgrid STATIC read_grid.F90)

add_executable(ocean ocean.F90 def_parallel_decomposition.F90)
target_compile_definitions(ocean PRIVATE -DDEPCOMP_APPLE)
target_link_libraries(ocean readgrid)


add_executable(atmos atmos.F90 def_parallel_decomposition.F90)
target_compile_definitions(atmos PRIVATE -DDEPCOMP_APPLE)
target_link_libraries(atmos readgrid)
