export COUPLE_ENV_VAR=$(dirname $(realpath ${BASH_SOURCE[0]}))
# No good on cray
export MPIDIR_ENV_VAR=/fs/ssm/main/opt/openmpi/openmpi-2.1.1/intel-2016.1.156--hpcx-1.8/ubuntu-14.04-amd64-64
export PATH=${MPIDIR_ENV_VAR}/bin:${PATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MPIDIR_ENV_VAR}/lib
# https://portal.science.gc.ca/confluence/display/PSECCC/Upgrade+1#Upgrade1-XC50.1

if [[ "${ORDENV_SETUP}" == "" ]] ; then
    echo "ERROR: Ordenv must be setup to work on oasis on PPP"
fi
export CC=mpicc
export CXX=mpicxx
export FC=mpif90

. ssmuse-sh -d main/opt/cmake/cmake-3.16.4
. ssmuse-sh -x hpco/exp/intelpsxe-cluster-19.0.3.199

# NETCDF_BASE=/fs/ssm/hpco/exp/netcdf-4.4.1.1/ubuntu-18.04-amd64-64
# NETCDF_FORTRAN=/fs/ssm/hpco/exp/hdf5-netcdf4/master/netcdf-fortran_4.4.5-intel-19.0.3.199-openmpi-3.1.2-static_ubuntu-18.04-amd64-64
# . r.load.dot rpn/code-tools/1.5.0
# . r.load.dot rpn/utils/19.7.0
# . r.load.dot rpn/libs/19.7.0
