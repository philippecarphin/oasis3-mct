export COUPLE_ENV_VAR=$(dirname $(realpath ${BASH_SOURCE[0]}))
# No good on cray
export MPIDIR_ENV_VAR=/fs/ssm/main/opt/openmpi/openmpi-2.1.1/intel-2016.1.156--hpcx-1.8/ubuntu-14.04-amd64-64
export PATH=${MPIDIR_ENV_VAR}/bin:${PATH}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${MPIDIR_ENV_VAR}/lib
# https://portal.science.gc.ca/confluence/display/PSECCC/Upgrade+1#Upgrade1-XC50.1

# To use the Intel compiler, you may have to run:
module swap PrgEnv-cray PrgEnv-intel
module load craype-x86-skylake
module load cray-netcdf
# JP gave me this but it doesn't work and I don't need it
# module load hdf5parallel/4.6.3.1
# but they're not sure I guess

export ORDENV_SITE_PROFILE=20210129
export ORDENV_COMM_PROFILE=eccc/20210309
export ORDENV_GROUP_PROFILE=eccc/cmc/1.9.4
. /fs/ssm/main/env/ordenv-boot-20201118.sh

. ssmuse-sh -d main/opt/cmake/cmake-3.16.4
export OASIS_HOST=daley
